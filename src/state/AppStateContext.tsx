import { DragItem } from 'DragItem'
import { createContext, useContext, Dispatch, FC } from 'react'

import { useImmerReducer } from 'use-immer'
import { Action } from './actions'
import { AppStateReducer } from './appStateReducer'

type Task = {
  id: string
  text: string
}

type List = {
  id: string
  text: string
  tasks: Task[]
}

export type AppState = {
  draggedItem: null
  lists: List[]
}

type AppStateContextProps = {
  draggedItem: DragItem | null
  lists: List[]
  getTaskByListId(id: string): Task[]
  dispatch: Dispatch<Action>
}

const AppStateContext = createContext<AppStateContextProps>(
  {} as AppStateContextProps
)

export const appData: AppState = {
  lists: [
    {
      id: '0',
      text: 'TO DO',
      tasks: [{ id: 'c0', text: 'Generate App Scaffold' }],
    },
    {
      id: '1',
      text: 'In Progress',
      tasks: [{ id: 'c1', text: 'Learn TypeScript' }],
    },
    {
      id: '1',
      text: 'In Progress',
      tasks: [{ id: 'c2', text: 'Begin to use static typing' }],
    },
  ],
}

export const AppStateProvider: FC = ({ children }) => {
  const [state, dispatch] = useImmerReducer(AppStateReducer, appData)
  const { draggedItem, lists } = state
  const getTaskByListId = (id: string) => {
    return lists.find((list) => list.id === id)?.tasks || []
  }

  return (
    <AppStateContext.Provider
      value={{ draggedItem, lists, getTaskByListId, dispatch }}
    >
      {children}
    </AppStateContext.Provider>
  )
}

export const useAppState = () => {
  return useContext(AppStateContext)
}
