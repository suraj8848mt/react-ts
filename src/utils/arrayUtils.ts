type Item = {
  id: string
}

// GENERIC FUNCTION TO FIND INDEX OF AN ITEM BY ID
export const findItemIndexById = <TItem extends Item>(
  items: TItem[],
  id: string
) => {
  return items.findIndex((item: TItem) => item.id === id)
}
// GENERIC FUNCTION TO REMOVE AN ITEM FROM AN ARRAY
export function removeItemAtIndex<TItem>(array: TItem[], index: number) {
  return [...array.slice(0, index), ...array.slice(index + 1)]
}

// GENERIC FUNCTION TO INSERT AN ITEM FROM AN ARRAY
export function insertItemAtIndex<TItem>(
  array: TItem[],
  item: TItem,
  index: number
) {
  return [...array.slice(0, index), item, ...array.slice(index)]
}

// GENERIC FUNCTION TO MOVE AN ITEM FROM AN ARRAY
export const moveItem = <TItem>(array: TItem[], from: number, to: number) => {
  const item = array[from]
  return insertItemAtIndex(removeItemAtIndex(array, from), item, to)
}
