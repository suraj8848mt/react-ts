import AddNewItem from '../Items/AddNewItem'
import React, { useRef } from 'react'
import { ColumnContainer, ColumnTitle } from '../../styles'
import Card from '../Card'

import { useItemDrag } from '../../utils/useItemDrag'

import { useAppState } from '../../state/AppStateContext'
import { addTask } from '../../state/actions'

type ColumnProps = {
  text: string
  children?: React.ReactNode
  id: string
}

const Column = ({ text, id }: ColumnProps) => {
  const ref = useRef<HTMLDivElement>(null)
  const { drag } = useItemDrag({ type: 'COLUMN', id, text })
  const { draggedItem, getTaskByListId, dispatch } = useAppState()
  const tasks = getTaskByListId(id)
  return (
    <>
      <ColumnContainer ref={ref}>
        <ColumnTitle>{text}</ColumnTitle>
        {tasks.map((task) => (
          <Card text={task.text} key={task.id} id={task.id} />
        ))}
        <AddNewItem
          toggleButtonText='+ Add Another Card'
          onAdd={(text) => dispatch(addTask(text, id))}
          dark
        />
      </ColumnContainer>
    </>
  )
}

export default Column
