import { AppContainer } from './styles'
import Column from './components/Column'
import AddNewItem from './components/Items/AddNewItem'

import { useAppState } from './state/AppStateContext'
import { addList } from './state/actions'

function App() {
  const { lists, dispatch } = useAppState()
  return (
    <div className='app'>
      <AppContainer>
        {lists.map((list) => (
          <Column key={list.id} text={list.text} id={list.id} />
        ))}
        <AddNewItem
          toggleButtonText='+ Add another list'
          onAdd={(text) => dispatch(addList(text))}
        />
      </AppContainer>
    </div>
  )
}

export default App
